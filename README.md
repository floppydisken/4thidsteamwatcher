4thid Steam discussions watcher
===============================
This project is dedicated to the 4thid to make recruiting a wee bit easier.  
All it does is poll the steamcommunites discussion and find the latest post. If the post is later than the one logged the program will spit out a "Update: ..." message.  
As of right now it's a command line application.  

The application will by default be checking the Arma 3 LFG discussion.

# How to run on from .exe file
WINDOWS ONLY!  
Go download the windows executable steamwatcher.exe file from **https://gitlab.com/floppydisken/4thidsteamwatcher/-/releases**
From there on you can simply double click on the program and it will run with default arguments or you open the file from PowerShell and insert your own arguments

PowerShell Example:
```powershell
./steamwatcher.exe --comment-max 10 --delay 120 --discussion https://steamcommunity.com/app/107410/discussions/21/
```
# How to run with python
The project is built with Python 3.7.
This being said I don't know if you can manage to run this project on earlier versions.

PowerShell:
```powershell
git pull https://gitlab.com/floppydisken/4thidsteamwatcher.git
cd 4thidsteamwatcher
python ./steamwatcher.py
```
# How to get help
Simply run the application with the --help argument

PowerShell:
```powershell
./steamwatcher.exe --help
```