#!/usr/bin/env python

import argparse
import time
import re
import sys
import pprint
import requests

from datetime import datetime
from bs4 import BeautifulSoup 


# Util decorator.
def auto_str(cls):
    def __str__(self):
        return "%s(%s)" % (
            type(self).__name__,
            ", ".join("%s=%s" % item for item in vars(self).items())
        )

    cls.__str__ = __str__
    return cls

class Post:
    def __init__(self, uid, url, timestamp, comment_count = 0):
        self.uid = uid
        self.url = url
        self.timestamp = timestamp
        self.comment_count = comment_count 

    def __str__(self):
        return f"Uid: {self.uid}; url: {self.url}; timestamp: {self.timestamp}"

    def __repr__(self):
        return f"Uid: {self.uid}; url: {self.url}; timestamp: {self.timestamp}"

@auto_str
class SteamWatcher:
    def __init__(self, discussion = "https://steamcommunity.com/app/107410/discussions/21/", ignore_pinned_posts = True, comment_min=0, comment_max=0, delay=30):
        self.discussion = discussion
        self.ignore_pinned_posts = ignore_pinned_posts
        self.comment_min = comment_min
        self.comment_max = comment_max
        self.delay = delay
        self.last_post = None
    
    def _remove_pinned_posts(self, soup):
        if self.ignore_pinned_posts:
            pinned_post_elements = soup.find_all(class_="sticky", recursive=True) # Remove pinned posts
            for elem in pinned_post_elements:
                elem.decompose()
        
        return soup

    def _find_first_post(self, soup):
        discussion_post_element = soup.find((lambda el : el.has_attr("data-gidforumtopic"))) # Get the first element e.g. the latest post
        uid = discussion_post_element["data-gidforumtopic"] 
        url = discussion_post_element.find(class_="forum_topic_overlay")["href"]
        timestamp = int(discussion_post_element.find(class_="forum_topic_lastpost")["data-timestamp"])
        comment_count = self._find_comment_count(url)
        return Post(
            uid, 
            url,
            timestamp,
            comment_count
        )
    
    def _find_comment_count(self, discussion_post_url):
            post_response = requests.get(discussion_post_url)
            post_soup = BeautifulSoup(post_response.text, "html.parser")
            return int(post_soup.find(id=re.compile(".fpagetotal")).text)

    def watch(self):
        try:
            response = requests.get(f"{self.discussion}?fp=0")
            soup = BeautifulSoup(response.text, "html.parser")
            self._remove_pinned_posts(soup)

            latest_discussion_post = self._find_first_post(soup)

            if (self.last_post is None \
                    or self.last_post.timestamp < latest_discussion_post.timestamp) \
                    and latest_discussion_post.comment_count >= self.comment_min \
                    and latest_discussion_post.comment_count <= self.comment_max:
                self.last_post = latest_discussion_post
                print(f"Update: {latest_discussion_post.url} {datetime.fromtimestamp(int(latest_discussion_post.timestamp))}, total comments: {latest_discussion_post.comment_count}")
        except Exception as e:
            print(f"Failed with exception {e}")
            print(f"Nothing fatal, continuing anyways")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Arma 3 steam community watcher")
    parser.add_argument(
        "-disc", "--discussion", 
        help="DEFAULT: https://steamcommunity.com/app/107410/discussions/21/; Which discussion to watch"
    )
    parser.add_argument(
        "-d", "--delay", type=int, 
        help="DEFAULT: 30; This watcher is an active \"poller\". This changes the delay between each poll."
    )
    parser.add_argument(
        "-max", "--comment-max", type=int, 
        help="DEFAULT: 0; Only show the post if it's below this max."
    )
    parser.add_argument(
        "-min", "--comment-min", type=int, 
        help="DEFUALT: 0; Only show the post if it's above this min.\nIncidentally if you use -min 0 and -max 0, you'll get new posts only."
    )
    parser.add_argument(
        "-i", "--ignore-pinned-posts", 
        help="DEFAULT: TRUE; Whether to ignore the top pinned posts or not."
    )

    args = parser.parse_args()

    watcher = SteamWatcher()

    if args.discussion:
        watcher.delay = args.discussion
    if args.comment_min:
        watcher.comment_min = args.comment_min
    if args.comment_max:
        watcher.comment_max = args.comment_max
    if args.delay:
        watcher.delay = args.delay
    
    if watcher.comment_min > watcher.comment_max:
        watcher.comment_max = watcher.comment_min

    print(f"Monitoring with {watcher}")
    while True:
        watcher.watch()
        time.sleep(watcher.delay)
